package com.laststar.test;

import java.util.Scanner;

public class Main {
    private char[] array1 = new char[]{'.', '!', '?'};
    private char[] array2 = new char[]{',', ';', ':'};
    private char symbolSingleQuoteMark = '\'';
    private char symbolDoubleQuoteMark = '\"';
    private String symbolTriplePoint = "...";

    public static void main(String[] args) {
        Main main = new Main();

        String text = "";
        text = main.init();
        System.out.println("Input text: " + text);
        text = main.funcCheckSingeQuestionMark(text);
        text = main.funcCheckArray2(text);
        text = main.funcCheckArray1(text);
        text = main.funcCheckDoubleQuestionMark(true, text);
        text = main.funcCheckDoubleQuestionMark(false, text);
        text = main.checkCapitalizationSymbols(text);
        System.out.println("New text: " + text);
    }

    private String init() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private String funcCheckDoubleQuestionMark(boolean fromStart, String text) {
        if(text.contains(Character.toString(symbolDoubleQuoteMark))) {
//        based on the example, we believe that double quote symbol we can see only at first and last position
            int index = fromStart ? 0 : text.length();
            int nextPosition = fromStart ? index + 1 : index - 1;
            if (fromStart ? nextPosition < text.length() : nextPosition > 0) {
                if (text.charAt(nextPosition) != 32 && fromStart) {
//                            after " we see not gap. First symbol need write at uppercase
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(text.charAt(0));
                    stringBuilder.append(Character.toUpperCase(text.charAt(nextPosition)));
                    stringBuilder.append(text.substring(index + 2, text.length()));
                    return stringBuilder.toString();
                } else {
//                            after " we see a few gap
                    int spaceIndexStart = nextPosition;
                    int spaceIndexEnd = nextPosition;
                    boolean isNotSpace = false;
                    int spaceIndex = fromStart ? index + 2 : index - 2;
                    while (!isNotSpace && (fromStart ? spaceIndex < text.length() : spaceIndex > 0)) {
                        if (text.charAt(spaceIndex) == 32) {
                            if (fromStart) {
                                spaceIndexEnd = spaceIndex;
                            } else {
                                spaceIndexStart = spaceIndex;
                            }
                        } else {
                            isNotSpace = true;
                        }
                        if (fromStart) {
                            spaceIndex++;
                        } else {
                            spaceIndex--;
                        }
                    }
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(text.substring(0, spaceIndexStart));
                    if (fromStart)
                        stringBuilder.append(Character.toUpperCase(text.charAt(spaceIndexEnd + 1)));
                    if (fromStart) {
                        stringBuilder.append(text.substring(spaceIndexEnd + 2, text.length()));
                    } else {
                        stringBuilder.append(text.substring(spaceIndexEnd, text.length()));
                    }
                    return stringBuilder.toString();
                }
            }
        }

        return text;
    }

    private String funcCheckSingeQuestionMark(String text) {
//         based on the example, we believe that single quote symbol we can see only in the word
        if(text.contains(Character.toString(symbolSingleQuoteMark))) {
            int index = text.indexOf(symbolSingleQuoteMark);
            StringBuilder stringBuilder = new StringBuilder();
            while (index < text.length() && index > 0) {
                int nextIndex = index + 1;
                int prevIndex = index - 1;
                boolean isSymbolNext = false;
                boolean isSymbolPrev = false;
                while (nextIndex < text.length() && prevIndex > 0 && (!isSymbolNext || !isSymbolPrev)) {
                    if (!isSymbolNext) {
                        if (text.charAt(nextIndex) == 32) {
                            nextIndex++;
                        } else {
                            isSymbolNext = true;
                        }
                    }
                    if (!isSymbolPrev) {
                        if (text.charAt(prevIndex) == 32) {
                            prevIndex--;
                        } else {
                            isSymbolPrev = true;
                        }
                    }
                }
                stringBuilder.append(text.substring(0, prevIndex + 1));
                stringBuilder.append(text.charAt(index));

                text = text.substring(nextIndex, text.length());
                index = text.indexOf(symbolSingleQuoteMark);
                if(index == -1) {
                    stringBuilder.append(text);
                }
            }
            return stringBuilder.toString();
        }

        return text;
    }

    private String funcCheckArray2(String text) {
        for(int i = 0; i < array2.length; i++) {
            if(text.contains(Character.toString(array2[i]))) {
                StringBuilder stringBuilder = new StringBuilder();
                int index = text.indexOf(array2[i]);
                while (index < text.length() && index > 0) {
                    int nextIndex = index + 1;
                    int prevIndex = index - 1;
                    boolean isSymbolNext = false;
                    boolean isSymbolPrev = false;
                    while (nextIndex < text.length() && prevIndex > 0 && (!isSymbolNext || !isSymbolPrev)) {
                        if (!isSymbolNext) {
                            if (text.charAt(nextIndex) == 32) {
                                nextIndex++;
                            } else {
                                isSymbolNext = true;
                            }
                        }
                        if (!isSymbolPrev) {
                            if (text.charAt(prevIndex) == 32) {
                                prevIndex--;
                            } else {
                                isSymbolPrev = true;
                            }
                        }
                    }
                    stringBuilder.append(text.substring(0, prevIndex + 1));
                    stringBuilder.append(text.charAt(index));
                    stringBuilder.append(" ");

                    text = text.substring(nextIndex, text.length());
                    index = text.indexOf(array2[i]);
                    if(index == -1) {
                        stringBuilder.append(text);
                    }
                }
                text = stringBuilder.toString();
            }
        }

        return text;
    }

    private boolean getIsTripePoint(char symbol, String text) {
        int index = text.indexOf(symbol);
        int indexTriplePoint = text.indexOf(symbolTriplePoint);
        return index == indexTriplePoint;
    }

    private String funcCheckArray1(String text) {
        for(int i = 0; i < array1.length; i++) {
            if(text.contains(Character.toString(array1[i]))) {
                StringBuilder stringBuilder = new StringBuilder();
                boolean isTriplePoint = getIsTripePoint(array1[i], text);
                int index = isTriplePoint ? text.indexOf(symbolTriplePoint) : text.indexOf(array1[i]);
                while (index < text.length() && index > 0) {
                    int nextIndex = isTriplePoint ? index + 3 : index + 1;
                    int prevIndex = index - 1;
                    boolean isSymbolNext = false;
                    boolean isSymbolPrev = false;
                    while (nextIndex < text.length() && prevIndex > 0 && (!isSymbolNext || !isSymbolPrev)) {
                        if (!isSymbolNext) {
                            if (text.charAt(nextIndex) == 32) {
                                nextIndex++;
                            } else {
                                isSymbolNext = true;
                            }
                        }
                        if (!isSymbolPrev) {
                            if (text.charAt(prevIndex) == 32) {
                                prevIndex--;
                            } else {
                                isSymbolPrev = true;
                            }
                        }
                    }
                    stringBuilder.append(text.substring(0, prevIndex + 1));
                    stringBuilder.append(isTriplePoint ? symbolTriplePoint : array1[i]);
                    if(nextIndex != text.length() - 1) {
                        stringBuilder.append(" ");
                    }

                    text = text.substring(nextIndex, text.length());
                    isTriplePoint = getIsTripePoint(array1[i], text);
                    index = isTriplePoint ? text.indexOf(symbolTriplePoint) : text.indexOf(array1[i]);
                    if (index == -1) {
                        stringBuilder.append(text);
                    }
                }
                text = stringBuilder.toString();
            }
        }

        return text;
    }

    private String checkCapitalizationSymbols(String text) {
        String[] arrays = text.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(arrays[0]);
        for(int i = 1; i < arrays.length; i++) {
            String prev = arrays[i - 1];
            String curr = arrays[i];
            char currFirstSymbol = curr.charAt(0);
            if(prev.charAt(prev.length() - 1) == '.' || prev.charAt(prev.length() - 1) == '!' || prev.charAt(prev.length() - 1) == '?') {
                curr = Character.toUpperCase(currFirstSymbol) + curr.substring(1, curr.length());
            } else {
                curr = Character.toLowerCase(currFirstSymbol) + curr.substring(1, curr.length());
            }
            if(i != arrays.length - 1) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(curr);
        }
        return stringBuilder.toString();
    }
}
